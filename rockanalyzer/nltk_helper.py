# -*- coding: utf-8 -*-
import re
from collections import defaultdict
from BeautifulSoup import BeautifulSoup
import nltk
import json

remove_chords = lambda (s): re.sub(r'\<b\>.*\<\/b\>', '', s)
remove_all_but_cyrillic = lambda (s): re.sub(ur'[^а-яА-ЯёЁ]', ' ', s)

def make_clean(lyrics):
    pass
tokenize = lambda(s): nltk.Text(nltk.word_tokenize(s))
normalize_text = lambda(text): [w.lower() for w in text]
vocabulary = lambda(words): sorted(set(words))


with open("data.json") as datafile:
    data = json.load(datafile,"utf-8")

for item in data:
    item['artist'] = BeautifulSoup(item['artist']).getText()
    item['title'] = BeautifulSoup(item['title']).getText()
    item['lyrics'] = BeautifulSoup(remove_chords(item['lyrics'])).getText()

data_grouped_by_artist = defaultdict(list)
for item in data:
    data_grouped_by_artist[item['artist']].append(
        tokenize(
        normalize_text(
        remove_all_but_cyrillic(
            item['lyrics']))))

# блять винда ебаная нихуя не работает из-за блядских кодировок

print()