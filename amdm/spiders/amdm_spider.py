__author__ = 'xome4ok'

from scrapy.spiders import CrawlSpider, Rule
from scrapy.linkextractors import LinkExtractor
from scrapy.loader.processors import TakeFirst
from scrapy.loader import ItemLoader
from scrapy.selector import HtmlXPathSelector
from amdm.items import SongItem

class SongLoader(ItemLoader):
    default_output_processor = TakeFirst()

class SongSpider(CrawlSpider):
    name = r"amdm"
    allowed_domains = [r"amdm.ru"]
    start_urls = [r"http://amdm.ru/chords/%s/" % x for x in range(55)]

    rules = (
        Rule(LinkExtractor(allow=(r'^http://amdm.ru/akkordi/\w+/$')), follow=True),
        Rule(LinkExtractor(allow=(r'/akkordi/\w+/\d+/\w+/')), callback='parse_item'),
    )

    def parse_item(self, response):
        hxs = HtmlXPathSelector(response)
        song = SongItem()

        song["artist"] = hxs.select(r"//span[@itemprop='byArtist']").extract()[0].encode('utf-8')
        song["title"] = hxs.select(r"//span[@itemprop='name']").extract()[0].encode('utf-8')
        song["lyrics"] = hxs.select(r"//pre[@itemprop='chordsBlock']").extract()[0].encode('utf-8')

        song['url'] = response.url

        return song
