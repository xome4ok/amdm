from scrapy import Item, Field

class SongItem(Item):
    # define the fields for your item here like:
    # name = scrapy.Field()

    artist = Field()
    title = Field()
    lyrics = Field()

    url = Field()