#!/usr/bin/env bash
cd ${0%/*}
{
  pip install -r requirements.txt
} 1> /dev/null
python ./main.py